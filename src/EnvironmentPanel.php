<?php

namespace JanHarsa\Tracy;

use Tracy\IBarPanel;

class EnvironmentPanel implements IBarPanel
{
    public function getTab()
    {
        ob_start(function () {
        });
        require __DIR__ . '/templates/tab.phtml';
        return ob_get_clean();
    }

    public function getPanel()
    {
        ob_start(function () {
        });
        require __DIR__ . '/templates/panel.phtml';
        return ob_get_clean();
    }

}